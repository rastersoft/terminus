/*
 * Copyright 2022 (C) Raster Software Vigo (Sergio Costas)
 *
 * This file is part of Terminus
 *
 * Terminus is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License.
 *
 * Terminus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

namespace Terminus {
    public enum MoveFocus {
        UP, DOWN, LEFT, RIGHT
    }
    public enum SplitAt {
        TOP, LEFT, BOTTOM, RIGHT, NONE
    }
    public enum EditingKeybindMode {
        NONE, KEYBIND, MACRO
    }
}
